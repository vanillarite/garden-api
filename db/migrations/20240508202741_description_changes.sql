-- +micrate Up
-- SQL in section 'Up' is executed when this migration is applied

DELETE FROM "description" WHERE "description"."description" = '';
ALTER TABLE "card" ADD COLUMN "has_description" INTEGER NOT NULL DEFAULT 0;
UPDATE "card" SET "has_description" = (SELECT EXISTS(SELECT 1 FROM "description" WHERE "description"."card_id" = "card"."id"));

-- +micrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE "card" DROP COLUMN "has_description";
