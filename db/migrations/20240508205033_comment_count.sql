-- +micrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE "card" ADD COLUMN "comment_count" INTEGER NOT NULL DEFAULT 0;
UPDATE "card" SET "comment_count" = (SELECT COUNT(*) FROM "comment" WHERE "comment"."card_id" = "card"."id");

-- +micrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE "card" DROP COLUMN "comment_count";
