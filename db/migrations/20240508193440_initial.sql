-- +micrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE "organization"(
    "id" TEXT PRIMARY KEY,
    "name" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "orgmembership"(
    "id" TEXT PRIMARY KEY,
    "organization_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,
    "level" INTEGER NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "user"(
    "id" TEXT PRIMARY KEY,
    "name" TEXT NOT NULL,
    "avatar" TEXT NOT NULL,
    "avatar_origin" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "level" INTEGER NOT NULL,
    "supabase_id" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "invite"(
    "id" TEXT PRIMARY KEY,
    "organization_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,
    "level" INTEGER NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "board"(
    "id" TEXT PRIMARY KEY,
    "organization_id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "column"(
    "id" TEXT PRIMARY KEY,
    "board_id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "idx" INTEGER NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "card"(
    "id" TEXT PRIMARY KEY,
    "column_id" TEXT NOT NULL,
    "board_id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "idx" INTEGER NOT NULL,
    "tags" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "description"(
    "id" TEXT PRIMARY KEY,
    "card_id" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "tag"(
    "id" TEXT PRIMARY KEY,
    "board_id" TEXT NOT NULL,
    "color" TEXT NOT NULL,
    "text" TEXT NOT NULL,
    "icon" TEXT NOT NULL,
    "idx" INTEGER NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;
CREATE TABLE "auditlog"(
    "id" TEXT PRIMARY KEY,
    "action" TEXT NOT NULL,
    "relevant_user" TEXT,
    "relevant_board" TEXT,
    "relevant_card" TEXT,
    "old_value" TEXT NOT NULL,
    "new_value" TEXT NOT NULL,
    "created_at" TEXT
) STRICT;
CREATE TABLE "comment"(
    "id" TEXT PRIMARY KEY,
    "card_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,
    "text" TEXT NOT NULL,
    "created_at" TEXT,
    "updated_at" TEXT
) STRICT;

-- +micrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE "organization";
DROP TABLE "orgmembership";
DROP TABLE "user";
DROP TABLE "invite";
DROP TABLE "board";
DROP TABLE "column";
DROP TABLE "card";
DROP TABLE "description";
DROP TABLE "tag";
DROP TABLE "auditlog";
DROP TABLE "comment";
