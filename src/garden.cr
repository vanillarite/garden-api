require "envy"
Envy.from_file "dev.env.yaml", ".env.yaml", "example.env.yaml", perm: 0o400

require "solhttp/config"
SolHTTP.config.env = ENV["SOLHTTP_ENV"]
SolHTTP.config.secret = ENV["SOLHTTP_SECRET"]
require "solhttp"

require "granite/adapter/sqlite"
Granite::Connections << Granite::Adapter::Sqlite.new(name: "sqlite", url: "sqlite3://./data.db")
require "granite"
require "json"
require "micrate"

require "./db"
require "./auth"
require "./response"
require "./supabase"
require "./api/board"
require "./api/card"

Log.setup do |c|
  backend = Log::IOBackend.new
  c.bind "*", :debug, backend
  c.bind "db", :info, backend
end

add_context_storage_type(Garden::DB::User)
add_context_storage_type(Garden::DB::Board)
add_context_storage_type(Garden::DB::Card)

module Garden
  extend self
  extend Response

  VERSION = {{ `shards version`.stringify.strip }}

  FOX_IDENT = ENV["FOX_IDENT"]?
  FOX_HREF  = ENV["FOX_HREF"]?

  def cors(ctx : HTTP::Server::Context) : Nil
    ctx.response.headers["Access-Control-Allow-Origin"] = ENV["CORS_ORIGIN"]
    ctx.response.headers["Access-Control-Allow-Headers"] = "Authorization, X-Garden-Client"
    ctx.response.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, PATCH, DELETE"
  end

  def validate(str : String, range : Range(Int32, Int32)) : Bool
    str.size.in?(range) && str.each_char.all?(&.printable?)
  end

  def validate_multiline(str : String, range : Range(Int32, Int32)) : Bool
    str.size.in?(range) && str.each_char.all? { |chr| chr == '\n' || chr.printable? }
  end

  get "/" do |ctx|
    next "garden by rymiel, version #{Garden::VERSION}. Powered by solhttp #{SolHTTP::VERSION}, kemal #{Kemal::VERSION}.#{FOX_IDENT ? " #{FOX_IDENT} (#{FOX_HREF})" : ""}"
  end

  get "/api/version" do |ctx|
    next Garden::VERSION
  end

  get "/api/me" do |ctx|
    request_user = Auth.auth_user ctx
    next if request_user.nil?

    next ok ctx, request_user
  end

  get "/api/me/boards" do |ctx|
    request_user = Auth.auth_user ctx
    next if request_user.nil?

    orgs = request_user.organizations.to_a
    boards = DB::Board.where("organization_id IN (SELECT organization_id FROM orgmembership WHERE user_id = ?)", request_user.id).select

    next ok ctx, {orgs: orgs, boards: boards}
  end

  get "/api/org" do |ctx|
    org_id = ctx.params.query["org"]? || next error ctx, "No org"
    request_by = Auth.auth_user_id ctx
    next if request_by.nil?
    next error ctx, 403, "You are not allowed to access this" unless DB::OrgMembership.exists?(organization_id: org_id, user_id: request_by)
    org = DB::Organization.find org_id
    next error ctx, "Invalid org" if org.nil?

    next ok ctx, {org: org, members: org.users}
  end

  get "/api/invite/:invite" do |ctx|
    invite_id = ctx.params.url["invite"]? || next error ctx, "No invite"
    invite = DB::Invite.find invite_id
    next error ctx, "Invalid invite" if invite.nil?

    next ok ctx, {org: invite.organization, user: invite.user}
  end

  get "/api/user" do |ctx|
    user_id = ctx.params.query["user"]? || next error ctx, "No user"
    next unless Auth.check_token ctx, user_id
    user = DB::User.find user_id
    next error ctx, "Invalid user" if user.nil?

    next ok ctx, {user: user, organizations: user.organizations}
  end

  post "/api/auth/supabase_login" do |ctx|
    jwt = ctx.params.body["jwt"]? || next error ctx, "No jwt"
    uuid = Supabase.verify_auth jwt, ctx
    next if uuid.nil?
    user = DB::User.find_by supabase_id: uuid.to_s
    if user.nil?
      next ok ctx, "register"
    end

    next ok ctx, Auth.mint_token user
  end

  get "/genid/:prefix" do |ctx|
    prefix = ctx.params.url["prefix"] || next "No prefix"
    prefix = HTML.escape prefix
    prefix + '.' + Nanoid.generate(size: 10)
  end

  # TODO: accept invites without registering, i guess, but that would only be necessary if this was ever public
  # but, we could just split this into register and join, both would need a valid invite, and the frontend would be
  # responsible for calling both if registering
  post "/api/auth/supabase_register" do |ctx|
    jwt = ctx.params.body["jwt"]? || next error ctx, "No jwt"
    name = ctx.params.body["name"]? || next error ctx, "No name"
    avatar_service = ctx.params.body["avatar"]? || next error ctx, "No avatar"
    invite_id = ctx.params.body["invite"]? || next error ctx, "No invite"

    next error ctx, "Invalid name" unless name.size.in?(2...64) && name.each_char.all?(&.printable?) # is this even strict

    uuid = Supabase.verify_auth jwt, ctx
    next if uuid.nil?

    next error ctx, "Invalid avatar" unless avatar_service.in? Supabase::SERVICES

    invite = DB::Invite.find invite_id
    next error ctx, "Invalid invite" if invite.nil?

    supabase_user = Supabase.get_user(uuid)
    email = supabase_user.email || next error ctx, "Incomplete user"

    next error ctx, "You are already registered" if DB::User.exists?(supabase_id: supabase_user.id)
    next error ctx, "Email already in use" if DB::User.exists?(email: email)

    avatar_url = Supabase.resolve_avatar_source(supabase_user, avatar_service) || ""
    user = DB::User.create!(
      name: name,
      avatar: avatar_url,
      avatar_origin: avatar_service,
      email: email,
      level: 0,
      supabase_id: supabase_user.id
    )

    DB::OrgMembership.create!(
      organization_id: invite.organization_id,
      user_id: user.id,
      level: invite.level,
    )

    next ok ctx, Auth.mint_token user
  end

  post "/api/auth/refresh" do |ctx|
    request_user = Auth.auth_user ctx
    next if request_user.nil?

    next ok ctx, Auth.mint_token request_user
  end

  get "/api/auth/identity" do |ctx|
    request_user = Auth.auth_user ctx
    next if request_user.nil?

    next ok ctx, Supabase.get_user UUID.new request_user.supabase_id
  end

  get "/api/relay/avatar/:user" do |ctx|
    user_id = ctx.params.url["user"]? || next error ctx, "No user"
    url = ctx.params.url["url"]? || next error ctx, "No url"
    user = DB::User.find user_id
    next error ctx, "Invalid user" if user.nil?
    next error ctx, "Invalid url" unless url == user.avatar

    HTTP::Client.get(user.avatar, HTTP::Headers{
      "User-Agent" => "Garden/#{Garden::VERSION} Media-Proxy Crystal/#{Crystal::VERSION} (garden@rymiel.space)",
    }) do |resp|
      ctx.response.status = resp.status
      {"Content-Type", "Date", "Expires", "Last-Modified"}.each do |header|
        if value = resp.headers[header]
          ctx.response.headers[header] = value
        end
      end
      IO.copy resp.body_io, ctx.response.output
      ctx.response.close
    end
  end

  options "/*" do |ctx|
    cors ctx
  end

  before_all do |ctx|
    cors ctx
  end

  error 404 do |ctx|
    cors ctx
  end

  API::BoardHandler.new
  API::CardHandler.new

  Micrate::DB.connection_url = "sqlite3://./data.db"
  Micrate::Cli.run_up

  SolHTTP.run
end
