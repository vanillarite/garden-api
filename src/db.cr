require "granite/adapter/sqlite"
require "granite"
require "nanoid"

abstract class Granite::Adapter::Base
  def direct(query : String, params = [] of String)
    elapsed_time = Time.measure do
      self.open do |db|
        db.exec query, args: params
      end
    end
    self.log query, elapsed_time, params
  end
end

module Garden::DB
  module Identifiable
    macro included
      column id : String, primary: true, auto: false

      before_save :set_new_id

      def gen_new_id
        PREFIX + '.' + Nanoid.generate(size: 10)
      end

      def set_new_id
        if @id.nil?
          @id = gen_new_id
        end
      end
    end

    macro finished
      {%
        prefixes = @type.includers.map(&.constant("PREFIX"))
        prefixes.each do |prefix|
          if (prefixes - [prefix]).size != prefixes.size - 1
            raise "Non-unique ID prefix: #{prefix}"
          end
        end
      %}
    end
  end

  class Organization < Granite::Base
    PREFIX = "org"
    include Identifiable

    connection sqlite
    table organization

    Nanoid.generate(size: 10)

    has_many :memberships, class_name: OrgMembership
    has_many :users, class_name: User, through: :orgmembership
    has_many :boards, class_name: Board
    has_many :invites, class_name: Invite

    column name : String
    timestamps
  end

  class OrgMembership < Granite::Base
    PREFIX = "om"
    include Identifiable

    connection sqlite
    table orgmembership

    belongs_to :organization, foreign_key: organization_id : String
    belongs_to :user, foreign_key: user_id : String

    column level : Int32
    timestamps
  end

  class User < Granite::Base
    PREFIX = "u"
    include Identifiable

    connection sqlite
    table user

    has_many :memberships, class_name: OrgMembership
    has_many :organizations, class_name: Organization, through: :orgmembership
    has_many :invites, class_name: Invite # invites created by this user

    column name : String
    column avatar : String
    @[JSON::Field(ignore_serialize: true)]
    column avatar_origin : String
    @[JSON::Field(ignore_serialize: true)]
    column email : String
    column level : Int32
    column supabase_id : String

    timestamps
  end

  class Invite < Granite::Base
    PREFIX = "i"
    include Identifiable

    connection sqlite
    table invite

    belongs_to :organization, foreign_key: organization_id : String
    belongs_to :user, foreign_key: user_id : String # note, this is the author of the invite

    column level : Int32

    timestamps
  end

  class Board < Granite::Base
    PREFIX = "b"
    include Identifiable

    connection sqlite
    table board

    belongs_to :organization, foreign_key: organization_id : String
    has_many :columns, class_name: Column
    has_many :cards, class_name: Card
    has_many :tags, class_name: Tag

    column name : String
    timestamps
  end

  class Column < Granite::Base
    PREFIX = "col"
    include Identifiable

    connection sqlite
    table column

    belongs_to :board, foreign_key: board_id : String
    has_many :cards, class_name: Card

    column name : String
    column idx : Int32
    timestamps
  end

  class Card < Granite::Base
    PREFIX = "c"
    include Identifiable

    connection sqlite
    table card

    belongs_to :column, foreign_key: column_id : String
    belongs_to :board, foreign_key: board_id : String
    has_one :description, class_name: Description
    has_many :comments, class_name: Comment

    column title : String
    column idx : Int32
    column tags : String = "" # Comma delimited list. Sue me
    column has_description : Bool = false
    column comment_count : Int32 = 0
    column deleted : Bool = false

    def tag_list : Set(String)
      tags.split(",", remove_empty: true).to_set
    end

    def tag_list=(arr : Set(String)) : Nil
      self.tags = arr.to_a.join(",")
    end

    timestamps
  end

  class Description < Granite::Base
    PREFIX = "d"
    include Identifiable

    connection sqlite
    table description

    belongs_to :card, foreign_key: card_id : String

    column description : String
    timestamps
  end

  class Tag < Granite::Base
    PREFIX = "t"
    include Identifiable

    connection sqlite
    table tag

    belongs_to :board, foreign_key: board_id : String

    column color : String
    column text : String
    column icon : String = ""
    column idx : Int32

    timestamps
  end

  class AuditLog < Granite::Base
    PREFIX = "a"
    include Identifiable

    connection sqlite
    table auditlog

    column action : String

    column relevant_user : String? = nil
    column relevant_board : String? = nil
    column relevant_card : String? = nil

    column old_value : String
    column new_value : String

    column created_at : Time
  end

  class Comment < Granite::Base
    PREFIX = "com"
    include Identifiable

    connection sqlite
    table comment

    belongs_to :card, foreign_key: card_id : String
    belongs_to :user, foreign_key: user_id : String

    column text : String

    timestamps
  end
end
