require "solhttp"
require "../response"
require "../audit_log"

module Garden::API
  class CardHandler < SolHTTP::Middleware::AbstractHandler
    include Response

    def initialize
      super "/api/board/:board/card/:card"
    end

    def hook_all(ctx : HTTP::Server::Context) : Bool
      request_user = Auth.auth_user ctx
      return false if request_user.nil?

      board_id = ctx.params.url["board"]?
      if board_id.nil?
        error ctx, "No board"
        return false
      end
      board = DB::Board.find board_id
      if board.nil?
        error ctx, "Invalid board"
        return false
      end

      unless DB::OrgMembership.exists?(organization_id: board.organization_id, user_id: request_user.id!)
        error ctx, 403, "You are not allowed to access this"
        return false
      end

      card_id = ctx.params.url["card"]?
      if card_id.nil?
        error ctx, "No card"
        return false
      end

      card = DB::Card.find card_id
      if card.nil? || card.board_id != board.id
        error ctx, "Invalid card"
        return false
      end

      ctx.set("user", request_user)
      ctx.set("board", board)
      ctx.set("card", card)

      return true
    end

    def user(ctx : HTTP::Server::Context) : DB::User
      ctx.get("user").as DB::User
    end

    def board(ctx : HTTP::Server::Context) : DB::Board
      ctx.get("board").as DB::Board
    end

    def card(ctx : HTTP::Server::Context) : DB::Card
      ctx.get("card").as DB::Card
    end

    def paths
      get "" do |ctx|
        card = card ctx
        column = card.column
        description = card.description
        comments = card.comments

        next ok ctx, {card: card, column: column, desc: description, comments: comments}
      end

      delete "" do |ctx|
        card = card ctx
        card.update!(deleted: true)

        next ok ctx, card
      end

      get "comments" do |ctx|
        card = card ctx

        next ok ctx, card.comments
      end

      post "comment" do |ctx|
        body = ctx.params.body["body"]? || next error ctx, "No body"
        body = body.gsub("\r\n", '\n')
        next error ctx, "Invalid body" unless Garden.validate_multiline(body, 0...2048) # is this even strict

        card = card ctx
        user = user ctx

        comment = DB::Comment.create!(card_id: card.id, user_id: user.id, text: body)
        DB::Card.adapter.direct(%(UPDATE "card" SET "comment_count" = "comment_count"+1 WHERE "id" = ?;), [card.id])

        AuditLog.fire("create.comment", "", body, ctx: ctx)

        next ok ctx, card.comments
      end

      post "description" do |ctx|
        new_desc = ctx.params.body["description"]? || next error ctx, "No description"
        new_desc = new_desc.gsub("\r\n", '\n').strip
        next error ctx, "Invalid description" unless Garden.validate_multiline(new_desc, 0...4096) # is this even strict

        card = card ctx
        description = card.description
        old_description = description.try(&.description)

        next error ctx, "No action to take" if description.nil? && new_desc.empty?

        if description.nil?
          card.update!(has_description: true)
          description = DB::Description.new(card_id: card.id)
        end

        if new_desc.empty?
          card.update!(has_description: false)
          description.destroy!
        else
          description.update!(description: new_desc)
        end

        user = user ctx
        board = board ctx
        if old_description.nil?
          AuditLog.fire("create.description", "", new_desc, ctx: ctx)
        elsif new_desc.empty?
          AuditLog.fire("delete.description", old_description, "", ctx: ctx)
        else
          AuditLog.fire("edit.description", old_description, new_desc, ctx: ctx)
        end

        next ok ctx, description
      end

      put "tag" do |ctx|
        tag_id = ctx.params.body["tag"]? || next error ctx, "No tag"
        board = board ctx
        next error ctx, "Invalid tag" unless DB::Tag.exists?(id: tag_id, board_id: board.id)

        card = card ctx
        old_tags = card.tags
        tags = card.tag_list

        card.tag_list = tags.add tag_id
        card.save!
        new_tags = card.tags

        AuditLog.fire("edit.tags", old_tags, new_tags, ctx: ctx)

        next ok ctx, card
      end

      delete "tag" do |ctx|
        tag_id = ctx.params.body["tag"]? || next error ctx, "No tag"
        board = board ctx
        next error ctx, "Invalid tag" unless DB::Tag.exists?(id: tag_id, board_id: board.id)

        card = card ctx
        old_tags = card.tags
        tags = card.tag_list

        tags.delete tag_id
        card.tag_list = tags
        card.save!
        new_tags = card.tags

        AuditLog.fire("edit.tags", old_tags, new_tags, ctx: ctx)

        next ok ctx, card
      end

      patch "tag" do |ctx|
        add_tag_list = ctx.params.body["add"]? || next error ctx, "No add"
        remove_tag_list = ctx.params.body["remove"]? || next error ctx, "No remove"
        board = board ctx

        add_tags = add_tag_list.split(",").reject(&.empty?)
        remove_tags = remove_tag_list.split(",").reject(&.empty?)
        all_tags = add_tags + remove_tags
        known_tags = board.tags
        known_tag_ids = known_tags.map(&.id!)

        next error ctx, "Invalid tag" unless all_tags.all?(&.in? known_tag_ids)
        next error ctx, "Duplicate tags" unless all_tags.all? { |i| all_tags.count(i) == 1 }

        card = card ctx
        old_tags = card.tags
        tags = card.tag_list

        card.tag_list = tags.concat(add_tags).subtract(remove_tags)
        card.save!
        new_tags = card.tags

        AuditLog.fire("edit.tags", old_tags, new_tags, ctx: ctx)

        next ok ctx, card
      end

      post "move" do |ctx|
        target_index = ctx.params.body["index"]?.try &.to_i? || next error ctx, "No index"
        column_id = ctx.params.body["column"]? || next error ctx, "No column"

        board = board ctx
        card = card ctx
        target_column = DB::Column.find(column_id)
        next error ctx, "Invalid column" if target_column.nil?
        next error ctx, "Invalid column" if target_column.board_id != board.id

        source_column = card.column_id == column_id ? target_column : card.column!
        source_index = card.idx

        max_index = DB::Card.where(column_id: target_column.id).order(idx: :desc).assembler.first.first?.try &.idx.+(1) || 1
        next error ctx, "Invalid index" if target_index > max_index || target_index < 0

        DB::Card.adapter.direct(%(BEGIN;))
        DB::Card.adapter.direct(%(UPDATE "card" SET "idx" = "idx"-1 WHERE "column_id" = ? AND "idx" >= ?;), [source_column.id, source_index])
        DB::Card.adapter.direct(%(UPDATE "card" SET "idx" = "idx"+1 WHERE "column_id" = ? AND "idx" >= ?;), [target_column.id, target_index])
        card.update!(idx: target_index, column_id: target_column.id)
        DB::Card.adapter.direct(%(COMMIT;))

        AuditLog.fire("move.card", "#{source_column.id}:#{source_index}", "#{target_column.id}:#{target_index}", ctx: ctx)

        next ok ctx, {card: card, column: target_column}
      end

      post "title" do |ctx|
        title = ctx.params.body["title"]?.try &.strip || next error ctx, "No title"
        next error ctx, "Invalid title" unless Garden.validate(title, 1...128) # is this even strict

        card = card ctx
        old_title = card.title
        card.update!(title: title)

        AuditLog.fire("edit.card", old_title, title, ctx: ctx)

        next ok ctx, card
      end
    end
  end
end
