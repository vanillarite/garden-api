require "solhttp"
require "../response"

module Garden::API
  class BoardHandler < SolHTTP::Middleware::AbstractHandler
    include Response

    def initialize
      super "/api/board/:board"
    end

    def hook_all(ctx : HTTP::Server::Context) : Bool
      request_user = Auth.auth_user ctx
      return false if request_user.nil?

      board_id = ctx.params.url["board"]?
      if board_id.nil?
        error ctx, "No board"
        return false
      end
      board = DB::Board.find board_id
      if board.nil?
        error ctx, "Invalid board"
        return false
      end

      unless DB::OrgMembership.exists?(organization_id: board.organization_id, user_id: request_user.id!)
        error ctx, 403, "You are not allowed to access this"
        return false
      end

      ctx.set("user", request_user)
      ctx.set("board", board)

      return true
    end

    def user(ctx : HTTP::Server::Context) : DB::User
      ctx.get("user").as DB::User
    end

    def board(ctx : HTTP::Server::Context) : DB::Board
      ctx.get("board").as DB::Board
    end

    def paths
      get "" do |ctx|
        board = board ctx
        organization = board.organization
        columns = board.columns.to_a
        cards = board.cards.to_a
        tags = board.tags.to_a
        members = organization.users

        next ok ctx, {org: organization, board: board, columns: columns, cards: cards, tags: tags, members: members}
      end

      post "tag" do |ctx|
        name = ctx.params.body["name"]?.try &.strip || next error ctx, "No name"
        next error ctx, "Invalid name" unless name.match_full(/[a-zA-Z0-9 \/_-]{3,20}/) # very strict for now
        color = ctx.params.body["color"]?.try &.strip || next error ctx, "No color"
        next error ctx, "Invalid color" unless color.match_full(/[a-z-]{3,16}/) # very strict for now

        board = board ctx
        last_index = DB::Tag.where(board_id: board.id).order(idx: :desc).assembler.first.first?.try &.idx.+(1) || 1
        tag = DB::Tag.create!(text: name, color: color, board_id: board.id, idx: last_index)

        AuditLog.fire("create.tag", "", "#{color}:#{name}", ctx: ctx)

        next ok ctx, tag
      end

      post "tag/:tag" do |ctx|
        tag_id = ctx.params.url["tag"]?.try &.strip || next error ctx, "No tag"
        name = ctx.params.body["name"]?.try &.strip || next error ctx, "No name"
        next error ctx, "Invalid name" unless name.match_full(/[a-zA-Z0-9 \/_-]{3,20}/) # very strict for now
        color = ctx.params.body["color"]?.try &.strip || next error ctx, "No color"
        next error ctx, "Invalid color" unless color.match_full(/[a-z-]{3,16}/) # very strict for now

        board = board ctx
        tag = DB::Tag.find(tag_id)
        next error ctx, "Invalid tag" if tag.nil?
        next error ctx, "Invalid tag" if tag.board_id != board.id

        old_color = tag.color
        old_name = tag.text

        tag.update!(text: name, color: color)

        AuditLog.fire("edit.tag", "#{old_color}:#{old_name}", "#{color}:#{name}", ctx: ctx)

        next ok ctx, tag
      end

      post "tag/:tag/move" do |ctx|
        tag_id = ctx.params.url["tag"]?.try &.strip || next error ctx, "No tag"
        index = ctx.params.body["index"]?.try &.to_i? || next error ctx, "No index"

        board = board ctx
        tag = DB::Tag.find(tag_id)
        next error ctx, "Invalid tag" if tag.nil?
        next error ctx, "Invalid tag" if tag.board_id != board.id
        old_index = tag.idx

        max_index = DB::Tag.where(board_id: board.id).order(idx: :desc).assembler.first.first?.try &.idx.+(1) || 1
        next error ctx, "Invalid index" if index > max_index || index < 0

        DB::Tag.adapter.direct(%(BEGIN;))
        DB::Tag.adapter.direct(%(UPDATE "tag" SET "idx" = "idx"-1 WHERE "board_id" = ? AND "idx" >= ?;), [board.id, old_index])
        DB::Tag.adapter.direct(%(UPDATE "tag" SET "idx" = "idx"+1 WHERE "board_id" = ? AND "idx" >= ?;), [board.id, index])
        tag.update!(idx: index)
        DB::Tag.adapter.direct(%(COMMIT;))

        AuditLog.fire("move.tag", "#{old_index}", "#{index}", ctx: ctx)

        next ok ctx, {tags: board.tags}
      end

      get "tags" do |ctx|
        board = board ctx
        tags = board.tags

        next ok ctx, {tags: tags}
      end

      post "column" do |ctx|
        name = ctx.params.body["name"]?.try &.strip || next error ctx, "No name"
        next error ctx, "Invalid name" unless name.match_full(/[a-zA-Z0-9 \/_-]{3,20}/) # very strict for now

        board = board ctx
        last_index = DB::Column.where(board_id: board.id).order(idx: :desc).assembler.first.first?.try &.idx.+(1) || 1

        new_column = DB::Column.create!(board_id: board.id, name: name, idx: last_index)

        AuditLog.fire("create.column", "", name, ctx: ctx)

        next ok ctx, new_column
      end

      post "column/:column/card" do |ctx|
        column_id = ctx.params.url["column"]? || next error ctx, "No column"
        title = ctx.params.body["title"]?.try &.strip || next error ctx, "No title"
        next error ctx, "Invalid title" unless title.size.in?(2...128) && title.each_char.all?(&.printable?) # is this even strict

        board = board ctx
        column = DB::Column.find column_id
        next error ctx, "Invalid column" if column.nil?
        next error ctx, "Invalid column" if column.board_id != board.id

        last_index = DB::Card.where(column_id: column.id).order(idx: :desc).assembler.first.first?.try &.idx.+(1) || 0

        new_card = DB::Card.create!(board_id: board.id, column_id: column.id, title: title, idx: last_index)

        AuditLog.fire("create.card", "", title, ctx: ctx, card: new_card)

        next ok ctx, new_card
      end
    end
  end
end
