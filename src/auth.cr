require "jwt"
require "db"
require "./response"

module Garden::Auth
  extend self

  TOKEN   = ENV["SOLHTTP_SECRET"]
  TIMEOUT = 1.day
  LEEWAY  = 10.minutes
  ALG     = JWT::Algorithm::HS256

  def mint_token(user : DB::User)
    iss = Time.utc.to_unix
    exp = iss + TIMEOUT.total_seconds.to_i64
    nbf = iss - LEEWAY.total_seconds.to_i64
    payload = {"exp" => exp, "nbf" => nbf, "iss" => iss, "sub" => user.id, "name" => user.name}
    JWT.encode payload, TOKEN, ALG
  end

  def check_token(ctx : HTTP::Server::Context, user_id : String) : Bool
    token = ctx.request.headers["Authorization"]?
    if token.nil?
      Response.error ctx, 401, "No authentication provided"
      return false
    end
    begin
      payload, header = JWT.decode(token, TOKEN, ALG)
      if payload["sub"]? == user_id
        return true
      else
        Response.error ctx, 403, "You are not allowed to access this"
      end
    rescue JWT::ExpiredSignatureError
      Response.error ctx, 401, "Authentication token has expired"
    rescue ex : JWT::Error
      Response.error ctx, 401, "Authentication token is invalid (#{ex})"
    rescue ex
      Response.error ctx, 500, "Unknown failure in authentication (#{ex})"
    end
    return false
  end

  def auth_user_id(ctx : HTTP::Server::Context) : String?
    token = ctx.request.headers["Authorization"]?
    if token.nil?
      Response.error ctx, 401, "No authentication provided"
      return nil
    end
    begin
      payload, header = JWT.decode(token, TOKEN, ALG)
      # If this is a valid token, the user should exist, but we are guarded by a rescue all clause anyway
      user_id = payload["sub"].as_s
      # user = DB::User.find_by!(id: user_id)
      return user_id
    rescue JWT::ExpiredSignatureError
      Response.error ctx, 401, "Authentication token has expired"
    rescue ex : JWT::Error
      Response.error ctx, 401, "Authentication token is invalid (#{ex})"
    rescue ex
      Response.error ctx, 500, "Unknown failure in authentication (#{ex})"
    end
    return nil
  end

  def auth_user(ctx : HTTP::Server::Context) : DB::User?
    request_by_id = Auth.auth_user_id ctx
    return nil if request_by_id.nil?
    request_user = DB::User.find request_by_id
    if request_user.nil?
      Response.error ctx, "Invalid user"
      return nil
    end
    return request_user
  end
end
