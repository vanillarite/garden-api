require "json"
require "http/client"
require "jwt"
require "digest/sha256"
require "./response"

module Garden::Supabase
  record UserMetadata,
    avatar_url : String,
    email : String,
    full_name : String,
    provider_id : String,
    custom_claims : Hash(String, JSON::Any)? do
    include JSON::Serializable
  end

  record UserIdentity,
    id : String,
    user_id : String,
    identity_data : UserMetadata?,
    identity_id : String,
    provider : String,
    created_at : String?,
    last_sign_in_at : String,
    updated_at : String? do
    include JSON::Serializable
  end

  record User,
    id : String,
    user_metadata : UserMetadata,
    aud : String,
    email : String?,
    created_at : String,
    confirmed_at : String?,
    email_confirmed_at : String?,
    last_sign_in_at : String?,
    role : String?,
    updated_at : String?,
    identities : Array(UserIdentity)? do
    include JSON::Serializable
  end

  PROJECT     = ENV["SUPABASE_PROJECT"]
  SERVICE_KEY = ENV["SUPABASE_KEY"]
  JWT_SECRET  = ENV["SUPABASE_JWT"]

  def self.get_user(id : UUID) : User
    resp = HTTP::Client.get("https://#{PROJECT}.supabase.co/auth/v1/admin/users/#{id}", HTTP::Headers{
      "apikey"        => SERVICE_KEY,
      "Authorization" => "Bearer #{SERVICE_KEY}",
      "User-Agent"    => "Garden/#{Garden::VERSION} Crystal/#{Crystal::VERSION} (garden@rymiel.space)",
    })
    User.from_json resp.body
  end

  def self.verify_auth(token : String, ctx : HTTP::Server::Context) : UUID?
    begin
      payload, header = JWT.decode token, JWT_SECRET, JWT::Algorithm::HS256 # why doesn't it get the algorithm from the header
      user_id = payload.as_h?.try &.["sub"]?.try &.as_s?
      if user_id
        return UUID.new user_id
      else
        Response.error ctx, 400, "Incomplete token"
      end
    rescue JWT::ExpiredSignatureError
      Response.error ctx, 401, "Authentication token has expired"
    rescue ex : JWT::Error
      Response.error ctx, 401, "Authentication token is invalid (#{ex})"
    rescue ex
      Response.error ctx, 500, "Unknown failure in authentication (#{ex})"
    end
  end

  SERVICES = ["google", "discord", "gravatar"]

  def self.resolve_avatar_source(user : User, service : String) : String?
    return nil unless service.in? SERVICES
    if service == "gravatar"
      email = user.email || return nil
      hash = Digest::SHA256.new.update(email.strip.downcase.to_slice).hexfinal
      return "https://www.gravatar.com/avatar/#{hash}"
    end
    return user.identities.try &.find(&.provider.== service).try &.identity_data.try &.avatar_url
  end
end
