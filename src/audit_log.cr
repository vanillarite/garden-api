module Garden::AuditLog
  def self.fire(action : String, old : String, new : String, *, user : DB::User? = nil, board : DB::Board? = nil, card : DB::Card? = nil, ctx : HTTP::Server::Context? = nil)
    user ||= ctx.try &.get?("user").try &.as? DB::User
    board ||= ctx.try &.get?("board").try &.as? DB::Board
    card ||= ctx.try &.get?("card").try &.as? DB::Card
    now = Time.utc
    spawn do
      DB::AuditLog.create!(
        relevant_user: user.try(&.id),
        relevant_board: board.try(&.id),
        relevant_card: card.try(&.id),
        action: action,
        old_value: old,
        new_value: new,
        created_at: now
      )
    end
  end
end
