require "kemal"

module Garden::Response
  extend self

  def error(ctx : HTTP::Server::Context, msg : String) : Nil
    error ctx, HTTP::Status::BAD_REQUEST, msg
  end

  def error(ctx : HTTP::Server::Context, error : Int32, msg : String) : Nil
    error ctx, HTTP::Status.new(error), msg
  end

  def error(ctx : HTTP::Server::Context, error : HTTP::Status, msg : String) : Nil
    ctx.response.reset
    ctx.response.content_type = "application/json"
    Garden.cors ctx
    ctx.response.status = error
    ctx.response << ({
      status: "error",
      error:  msg,
    }.to_json)
    ctx.response.close
  end

  def ok(ctx : HTTP::Server::Context, body) : Nil
    ctx.response.reset
    ctx.response.content_type = "application/json"
    Garden.cors ctx
    ctx.response.status = HTTP::Status::OK
    ctx.response << ({
      status:   "ok",
      response: body,
    }.to_json)
    ctx.response.close
  end
end
